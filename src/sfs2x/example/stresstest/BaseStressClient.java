package sfs2x.example.stresstest;

public abstract class BaseStressClient
{
	// Reference to the main class
	private StressTestReplicator shell;
	
	/*
	 * This is implemented in the child class running the test
	 * The method is called by the test replicator to run the custom client code
	 */
	public abstract void startUp();
	
	public void setShell(StressTestReplicator shell)
	{
		this.shell = shell;
	}
	
	/*
	 * This must be called by the child class when the SFS client disconnects.
	 */
	protected void onShutDown(BaseStressClient client)
	{
		shell.handleClientDisconnect(client);
	}
}
