package sfs2x.example.stresstest;

import java.io.FileInputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class StressTestReplicator
{
	private final List<BaseStressClient> clients;
	private final ScheduledThreadPoolExecutor generator;
	
	private String clientClassName;		// name of the client class
	private int generationSpeed = 250; 	// interval between each client is connection
	private int totalCCU = 50;			// # of CCU
	
	private Class<?> clientClass;
	private ScheduledFuture<?> generationTask;
	
	public StressTestReplicator(Properties config)
    {
		clients = new LinkedList<>();
		generator = new ScheduledThreadPoolExecutor(1);
		
		clientClassName = config.getProperty("clientClassName");
		
		try { generationSpeed = Integer.parseInt(config.getProperty("generationSpeed")); } catch (NumberFormatException e ) {};
		try { totalCCU = Integer.parseInt(config.getProperty("totalCCU")); } catch (NumberFormatException e ) {};
			
		System.out.printf("%s, %s, %s\n", clientClassName, generationSpeed, totalCCU);
		
		try
		{
			// Load main client class
			clientClass = Class.forName(clientClassName);
			
			// Prepare generation
			generationTask = generator.scheduleAtFixedRate(new GeneratorRunner(), 0, generationSpeed, TimeUnit.MILLISECONDS);
		}
		catch (ClassNotFoundException e) 
		{ 
			System.out.println("Specified Client class: " + clientClassName + " not found! Quitting.");
		}
    }
	
	void handleClientDisconnect(BaseStressClient client)
	{
		synchronized (clients)
        {
	        clients.remove(client);
        }

		if (clients.size() == 0)
		{
			System.out.println("===== TEST COMPLETE =====");
			System.exit(0);
		}
	}
	
	public static void main(String[] args) throws Exception
    {
		String defaultCfg = args.length > 0 ? args[0] : "config.properties";
		
		Properties props = new Properties();
		props.load(new FileInputStream(defaultCfg));
		
	    new StressTestReplicator(props);
    }
	
	//================================================================================================
	
	private class GeneratorRunner implements Runnable
	{
		@Override
		public void run()
		{
			try
            {
	            if (clients.size() < totalCCU)
	            	startupNewClient();
	            else
	            	generationTask.cancel(true);
            }
            catch (Exception e)
            {
	            System.out.println("ERROR Generating client: " + e.getMessage());
            }
		}
		
		private void startupNewClient() throws Exception
		{
			BaseStressClient client = (BaseStressClient) clientClass.newInstance();
			
			synchronized (clients)
            {
				clients.add(client);
            }
			
			client.setShell(StressTestReplicator.this);
			
			client.startUp();
		}
	}
}
