package sfs2x.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import sfs2x.client.SmartFox;
import sfs2x.client.core.BaseEvent;
import sfs2x.client.core.IEventListener;
import sfs2x.client.core.SFSEvent;
import sfs2x.client.entities.Room;
import sfs2x.client.requests.ExtensionRequest;
import sfs2x.client.requests.LoginRequest;

import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.exceptions.SFSException;

public class BotSpawner implements Runnable {
	Executor executor = null;
	static ExecutorService executorService = null;
	static ScheduledExecutorService scheduleExecutorService = null;
	static BotSpawner botSpawner = null;
	SmartFox sfs = null;
	int reconnectionAttempts = 0;
	boolean reconnecting = false;
	Future<?> runner = null;
	boolean idDisconnectionDetected = false;
	int countKeepAliveTimer = 0;
	ScheduledFuture<?> reconnectTimer = null;
	private ScheduledFuture<?> timer = null;
	private Date lastTime = new Date();
	private ConcurrentHashMap<String, Integer> roomWiseNumOfBots = new ConcurrentHashMap<String, Integer>();
	private ConcurrentHashMap<String, CopyOnWriteArrayList<BotAccount>> roomWiseBots = new ConcurrentHashMap<String, CopyOnWriteArrayList<BotAccount>>();
	private CopyOnWriteArrayList<BotAccount> botAccountMap = new CopyOnWriteArrayList<BotAccount>();

	public static void main(String[] args) {
		executorService = Executors.newFixedThreadPool(10);
		scheduleExecutorService = Executors.newScheduledThreadPool(25);
		botSpawner = new BotSpawner();
	}
	
	 public void loadBotNames()
	  {
	    String data = "";
	   BufferedReader reader =null;
	     File botIdFile = new File("./config/usersNew.txt.csv");
	    try
	    {
	      reader = new BufferedReader(new FileReader(botIdFile));
	    }
	    catch (FileNotFoundException e)
	    {
	      e.printStackTrace();
	    }
	    try
	    {
	      if (reader != null)
	      {
	        reader.readLine();
	        while ((data = reader.readLine()) != null)
	        {
	          String[] userAcc = data.split(",");
	          BotAccount bot = new BotAccount();
	          bot.setUsername(userAcc[0]);
	          bot.setPassword(userAcc[1]);
	          bot.setStatus(Boolean.valueOf(true));
	          botAccountMap.add(bot);
	        }
	      }
	    }
	    catch (IOException e)
	    {
	      e.printStackTrace();
	    }
	  }

	public BotSpawner() {
		loadBotNames();
		fillBots();
		init();
	}

	private void fillBots(){
		Properties p = new Properties();
	    try
	    {
	      if (p.isEmpty()) {
	        p.load(new FileInputStream("./config/bot.properties"));
	      }
	    }
	    catch (FileNotFoundException e)
	    {
	      e.printStackTrace();
	    }
	    catch (IOException e)
	    {
	      e.printStackTrace();
	    }
		String roomConfigIds = p.getProperty("ROOM_CONFIG_ID");
		String numofBotsPerRoom = p.getProperty("NUM_OF_BOTS");
		List<Integer> numOfBots = new ArrayList<Integer>();
		for(String noOfBot :numofBotsPerRoom.split(",")){
			numOfBots.add(Integer.parseInt(noOfBot));
		}
		String[] rooms =roomConfigIds.split(",");
		for(int i=0;i<rooms.length;i++){
			roomWiseNumOfBots.put(rooms[i], numOfBots.get(i));
		}
	}
	
	private String getCfgPath() {
		return System.getProperty("user.dir") + "/config/sfs-config.xml";
	}

	public void init() {
		sfs = new SmartFox();
		try {
			this.sfs.loadConfig(getCfgPath(), true);
		} catch (Exception localException) {
		}

		this.sfs.addEventListener(SFSEvent.CONNECTION, new IEventListener() {
			@Override
			public void dispatch(BaseEvent evt) throws SFSException {
				System.out.println("Current Thread Name CONNECTION : "
						+ Thread.currentThread().getName());

				System.out.println("BotSpawner.this.reconnectionAttempts : "
						+ botSpawner.reconnectionAttempts + "; DateTime : "
						+ new Date());

				Map params = evt.getArguments();
				boolean isSuccess = ((Boolean) params.get("success"))
						.booleanValue();
				if (isSuccess) {
					System.out
							.println("BotSpawner.Connection established; Now Doing Login : ");
					if (botSpawner.reconnecting) {
						BotSpawner.this.stopReconnectionTimer();
					}
					BotSpawner.this.sfs.send(new LoginRequest("nitish", "",
							BotSpawner.this.sfs.getCurrentZone()));
				} else {
					System.out.println("BotSpawner.Connection failed");
				}
			}

		});

		this.sfs.addEventListener(SFSEvent.CONNECTION_LOST,
				new IEventListener() {

					@Override
					public void dispatch(BaseEvent arg0) throws SFSException {
						// TODO Auto-generated method stub
						botSpawner.idDisconnectionDetected = true;
						cleanUp();

					}
				});
		this.sfs.addEventListener(SFSEvent.LOGIN, new IEventListener() {

			@Override
			public void dispatch(BaseEvent arg0) throws SFSException {
				// TODO Auto-generated method stub
				if (!botSpawner.reconnecting) {
					try {
						botSpawner.runner = BotSpawner.this.executorService
								.submit(botSpawner);
					} catch (Exception e) {
						System.out
								.println("************************* During Login ****************************");
						System.out.println(e);
					}
				}

				if (botSpawner.reconnecting) {
					BotSpawner.this.stopReconnectionTimer();
				}
				
				botSpawner.timer = BotSpawner.scheduleExecutorService
						.scheduleAtFixedRate(new KeepAlive(), 1L, 5L,
								TimeUnit.SECONDS);

			}
		});
		this.sfs.addEventListener(SFSEvent.LOGIN_ERROR, new IEventListener() {

			@Override
			public void dispatch(BaseEvent arg0) throws SFSException {
				// TODO Auto-generated method stub
				System.out.println("Getting error during login for demon bot");
				System.exit(0);
			}
		});
		this.sfs.addEventListener("extensionResponse", new IEventListener()
	    {
	      public void dispatch(BaseEvent evt)
	        throws SFSException
	      {
	        System.out.println("Current Thread Name EXTENSION_RESPONSE : " + Thread.currentThread().getName());
	        
	        Map params = evt.getArguments();
	        ISFSObject data = (ISFSObject)params.get("params");
	        if ((params != null) && (params.get("cmd").equals("lobby.keepAlive")))
	        {
	        	botSpawner.countKeepAliveTimer = 0;
	          botSpawner.reconnectionAttempts =0;
	          System.out.println("BotSpawner game.keepAlive==========" + new Date() + "============" + BotSpawner.this.countKeepAliveTimer);
	        }
	      }
	    });
	}

	private void cleanUp() {
		// Remove listeners
		sfs.removeAllEventListeners();

		// Stop task
	}

	class KeepAlive implements Runnable {
		KeepAlive() {
			System.out.println("Strating Keep ALive");
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			if (!botSpawner.idDisconnectionDetected) {
				if (botSpawner.countKeepAliveTimer >= 3) {
					botSpawner.idDisconnectionDetected = true;
					// start reconnecting
					botSpawner.startReconnectTimer();
					return;
				}
			}
			SFSObject data = new SFSObject();
			BotSpawner.this.sendExtensionRequest("lobby.keepAlive", data, null);
			botSpawner.countKeepAliveTimer= botSpawner.countKeepAliveTimer+1;
			long diff = new Date().getTime() - botSpawner.lastTime.getTime();
			if(botSpawner.reconnecting || diff > 9000L){
				   botSpawner.runner = BotSpawner.executorService.submit(botSpawner);
			}

		}

	}

	public void startReconnectTimer() {
		if (!botSpawner.idDisconnectionDetected) {
			System.out.println("Already having a connection");
			return;
		}
		botSpawner.countKeepAliveTimer =0;
		if (botSpawner.reconnectTimer==null || botSpawner.reconnectTimer.isDone()) {
			botSpawner.reconnectTimer = BotSpawner.this.scheduleExecutorService
					.scheduleAtFixedRate(new Reconnect(), 1L, 12L,
							TimeUnit.SECONDS);
		}
	}

	public void stopReconnectionTimer() {
		idDisconnectionDetected = false;
		reconnectionAttempts = 0;
		if (reconnectTimer != null) {
			reconnectTimer.cancel(false);
			reconnectTimer = null;
			System.out.println("BotSpawner.reconnectTimer cancelled");
		}
	}

	class Reconnect implements Runnable {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			try{
				botSpawner.reconnectionAttempts = botSpawner.reconnectionAttempts+1;
				System.out.println("botSpwaner count for reconnection attempt" + botSpawner.reconnectionAttempts);
			if(botSpawner.reconnectionAttempts > 2){
				botSpawner.cleanUp();
				BotSpawner.this.executorService.shutdown();
				BotSpawner.this.scheduleExecutorService.shutdown();
				System.exit(0);
			}
			botSpawner.reconnecting = true;
			botSpawner.sfs = null;
			botSpawner.init();
			
			}catch(Exception e){
				System.out.println(e);
			}
		}

	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		for (;;) {
			try {
				botSpawner.lastTime = new Date();
				addBot();
			} catch (Exception e) {
				System.out.println(e.toString());
			}
		}

	}
	
	private void addBot(){
		if(roomWiseNumOfBots!=null && !roomWiseNumOfBots.isEmpty()){
			CopyOnWriteArrayList<BotAccount> botsPerRoom = null;
			int count=0;
			for(Entry<String, Integer> entry : roomWiseNumOfBots.entrySet()){
				botsPerRoom = roomWiseBots.get(entry.getKey());
				if(botsPerRoom !=null){
					count = botsPerRoom.size();
				}
				if(count < entry.getValue()){
					for(BotAccount bot :botAccountMap){
						if(!bot.getStatus()){
							//add bot to simpleBotClient
							botsPerRoom.add(bot);
							//	roomWiseBots
							//executorService.submit(arg0); //Execute simple bot client
						}
					}
				}
			}
		}
	}

	private void sendExtensionRequest(String command, ISFSObject data, Room room) {
		if ((this.sfs != null) && (this.sfs.getMySelf() != null)
				&& (this.sfs.isConnected())) {
			if (room != null) {
				this.sfs.send(new ExtensionRequest(command, data, room));
			} else {
				this.sfs.send(new ExtensionRequest(command, data, null));
			}
		}
	}

	public CopyOnWriteArrayList<BotAccount> getBotAccountMap() {
		return botAccountMap;
	}

	public void setBotAccountMap(CopyOnWriteArrayList<BotAccount> botAccountMap) {
		this.botAccountMap = botAccountMap;
	}
}
