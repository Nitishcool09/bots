package sfs2x.main;

public class BotAccount
{
  private String username;
  private String password;
  private Boolean status;
  
  public void setUsername(String username)
  {
    this.username = username;
  }
  
  public String getUsername()
  {
    return this.username;
  }
  
  public void setPassword(String password)
  {
    this.password = password;
  }
  
  public String getPassword()
  {
    return this.password;
  }
  
  public void setStatus(Boolean status)
  {
    this.status = status;
  }
  
  public Boolean getStatus()
  {
    return this.status;
  }
}

